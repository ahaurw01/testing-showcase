'use strict';

/**
 * A "database" for our cats app.
 */
var db = {
  cats: [{
    id: 1,
    name: 'Mittens',
    nickname: 'Mitt',
    age: 7,
    color: 'tabby',
  }, {
    id: 2,
    name: 'George',
    nickname: 'Georgey',
    age: 2,
    color: 'black',
  }, {
    id: 3,
    name: 'Sir Darius McSnuggle',
    nickname: 'Lil Snuggs',
    age: 7,
    color: 'orange',
  }]
};

module.exports = db;
