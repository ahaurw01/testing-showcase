'use strict';

var _ = require('lodash');
var db = require('../db');

/**
 * Business logic for cat stuff.
 */
var catsBusiness = {
  /**
   * Get all known cats.
   *
   * @return {Array<Object>} Array of cats.
   */
  getAllCats: function () {
    return db.cats;
  },

  /**
   * Get single cat by identifier.
   *
   * @param {Number} id   Cat identifier.
   *
   * @return {Object|void} Cat record or undefined if does not exist.
   */
  getCatById: function (id) {
    return _.find(db.cats, 'id', id);
  },
};

module.exports = catsBusiness;
