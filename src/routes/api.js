'use strict';

var express = require('express');
var router = express.Router();

var catsBusiness = require('../business/cats');

/**
 * GET /api/cats
 *
 * Retrieve a list of all cats.
 *
 * { cats: [{...}, ..., {...}] }
 */
router.get('/cats', function (req, res, next) {
  var cats = catsBusiness.getAllCats();
  res.send({ cats: cats });
});

/**
 * GET /api/cats/:id
 *
 * Retrieve one cat by its id.
 *
 * { cat: {...} }
 */
router.get('/cats/:id', function (req, res, next) {
  var catId = Number(req.params.id);
  var cat = catsBusiness.getCatById(catId);
  res.send({ cat: cat });
});

module.exports = router;
