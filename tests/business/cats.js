'use strict';

var expect = require('chai').expect;
var sinon = require('sinon');

var catsBusiness = require('../../src/business/cats');
var db = require('../../src/db');

describe('Business', function () {

  describe('Cats', function () {

    var sandbox;

    beforeEach(function () {
      sandbox = sinon.sandbox.create();
    });

    afterEach(function () {
      sandbox.restore();
    });

    describe('getAllCats', function () {

      it('should get all cats from the database', function () {
        var cats = ['cat 1', 'cat 2', 'cat 3'];
        sandbox.stub(db, 'cats', cats);

        var result = catsBusiness.getAllCats();

        expect(result).to.eql(cats);
      });

    });

    describe('getCatById', function () {

      beforeEach(function () {
        var cats = [{
          id: 1
        }];
        sandbox.stub(db, 'cats', cats);
      });

      context('for existing cat', function () {
        it('should return the cat', function () {
          var result = catsBusiness.getCatById(1);

          expect(result).to.eql({
            id: 1
          });
        });
      });

      context('for nonexistent cat', function () {
        it('should return undefined', function () {
          var result = catsBusiness.getCatById(42);

          expect(result).to.be.undefined;
        });
      });

    });

  });

});
