'use strict';

var request = require('supertest');
var expect = require('chai').expect;
var sinon = require('sinon');

var app = require('../../src/app');
var db = require('../../src/db');

describe('Routes', function () {

  describe('API', function () {

    var sandbox;

    beforeEach(function () {
      sandbox = sinon.sandbox.create();

      // Stub cats in database.
      sandbox.stub(db, 'cats', [{
        id: 1
      }, {
        id: 2
      }]);
    });

    afterEach(function () {
      sandbox.restore();
    });

    describe('GET /api/cats', function () {

      it('should send back all cats', function (done) {
        request(app)
          .get('/api/cats')
          .expect(200)
          .end(function (err, res) {
            if (err) {
              return done(err);
            }

            expect(res.body)
              .to.have.property('cats')
              .that.eql([{ id: 1 }, { id: 2 }]);
            done();
          });
      });

    });

    describe('GET /api/cats/:id', function () {

      it('should send back the requested cat', function (done) {
        request(app)
          .get('/api/cats/1')
          .expect(200)
          .end(function (err, res) {
            if (err) {
              return done(err);
            }

            expect(res.body)
              .to.have.property('cat')
              .that.eql({ id: 1 });
            done();
          });
      });

    });

  });

});
